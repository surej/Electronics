**The OpenOximeter is currently in an early prototype stage. It is never safe to build your own medical equipment. This project is open to encourage experts to work together to build the best and most adaptable solution.**

# Electronics

The electronics to interface from a PPG Sensor to an Arduino and OLED display

## Design Format

The schematic and layout files are in Eagle format

## Assembly instructions

The docs directory contains assembly instructions. You can view a hosted version of the documentation [here](https://openoximeter.gitlab.io/Electronics).

